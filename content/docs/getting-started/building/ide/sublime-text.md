---
title: "Sublime Text"
description: "An advanced text editor that supports many languages."
weight: 6
authors:
    - SPDX-FileCopyrightText: 2024 Kristen McWilliam <kmcwilliampublic@gmail.com>
SPDX-License-Identifier: CC-BY-SA-4.0
---

{{< alert color="warning" title="⚠️ kdesrc-build is no longer supported" >}}

</br>
<details>
<summary>Click to see more information</summary></br>

[kdesrc-build](https://invent.kde.org/sdk/kdesrc-build),
the tool that was used previously for this tutorial, is no longer supported.

While the tool is stable and still works for our veteran developers, if you are starting out with KDE development now, we recommend that you switch to
[kde-builder](https://kde-builder.kde.org/).

However, no plugin for Sublime Text to integrate kde-builder exists at this point in time (2024-10).
If you'd like to add more information to this page, please make a merge request on the [KDE Developer repository](https://invent.kde.org/documentation/develop-kde-org).

See also [Where to find the development team]({{< ref "help-developers" >}}).

</details>

{{< /alert >}}


You can install the [kdesrc-build plugin](https://github.com/ratijas/kdesrc-build-sublime) for Sublime Text.
