module develop-kde-org

go 1.19

require (
	invent.kde.org/websites/hugo-bootstrap v0.0.0-20230310220713-2c58a2e72e20 // indirect
	invent.kde.org/websites/hugo-kde v0.0.0-20240910113839-c508067d3dd7 // indirect
)
